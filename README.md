README
======

This is a simple contacts app front-end written in React.js.

Install & Run
-------------

1.  Clone repo & change to local directory
2.  `npm install`
3.  Set the correct API URL in app.js within the `ContactList` element (this should be the URL to the serivces in my [`contacts` repo](https://bitbucket.org/elgertam/contacts))
4.  `npm start`