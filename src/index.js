var injectTapEventPlugin = require('react-tap-event-plugin');

injectTapEventPlugin();

var React = require('react');
var ReactDOM = require('react-dom');
var App = require('./app');

ReactDOM.render(<App />, document.getElementById('content'));
