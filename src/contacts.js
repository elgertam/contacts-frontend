var React = require('react');
var $ = require('jquery');
var _ = require('lodash');

import ContentAdd from 'material-ui/lib/svg-icons/content/add';
var {Card, CardActions, CardHeader, CardText, FlatButton, Table, TableHeader, TableHeaderColumn, TableBody, TableRow, TableRowColumn, TextField, FloatingActionButton, RaisedButton} = require('material-ui');

var ContactList = React.createClass({
    getInitialState: function() {
        return {contacts: [], filterString: ''};
    },
    loadContactsFromApi: function() {
        $.ajax({
            url: this.props.url,
            dataType: 'json',
            cache: false,
            success: function(response) {
                this.setState({contacts: response.data, filterString: ''});
            }.bind(this),
            error: function(xhr, status, err) {
                console.error(this.props.url, status, err.toString());
            }.bind(this)
        });
    },
    changeFilter: function (newFilter) {
        this.setState({contacts: this.state.contacts, filterString: newFilter})
    },
    addContact: function(contact) {
        var contacts = this.state.contacts;
        contacts.concat([contact]);
        this.setState({contacts: contacts, filterString: ''});
        $.ajax({
            url: this.props.url,
            type: 'POST',
            dataType: 'json',
            data: JSON.stringify(contact),
            contentType: "application/json; charset=utf-8",
            cache: false,
            success: function(response) {
                this.loadContactsFromApi();
            }.bind(this),
            error: function(xhr, status, err) {
                console.error(this.props.url, status, err.toString());
            }.bind(this)
        });
    },
    removeContact: function(contactId) {
        var contacts = this.state.contacts;
        _.remove(contacts, function(contact) {
            return contact.id === contactId;
        });

        this.setState({contacts: contacts, filterString: this.state.filterString}),
        $.ajax({
            url: this.props.url + '/' + contactId,
            type: 'DELETE',
            dataType: 'json',
            cache: false,
            success: function(response) {
                this.loadContactsFromApi();
            }.bind(this),
            error: function(xhr, status, err) {
                console.error(this.props.url, status, err.toString());
            }.bind(this)
        });
    },
    componentDidMount: function() {
        this.loadContactsFromApi();
    },
    render: function() {
        var contactNodes = this.state.contacts
            .filter(function(contact){
                return contact.name.toLowerCase().indexOf(this.state.filterString.toLowerCase()) != -1;
            }.bind(this))
            .sort(function(a, b){
                var nameA = a.name.split(' ').slice(-1)[0];
                var nameB = b.name.split(' ').slice(-1)[0];

                return nameA.localeCompare(nameB);
            })
            .map(function(contact) {
                return (
                    <Contact key={contact.id} name={contact.name} details={contact.details} contactId={contact.id} callbackOnDelete={this.removeContact} />
                );
            }.bind(this));
        return (
            <div>
                <h1>Contacts App</h1>
                <SearchBar callbackOnChange={this.changeFilter} />
                <div>{contactNodes}</div>
                <ContactForm callbackOnSubmit={this.addContact} />
            </div>
        );
    }
});

var SearchBar = React.createClass({
    getInitialState: function() {
        return {
            searchString: ''
        };
    },
    handleOnChange: function(e) {
        this.setState({searchString: e.target.value});
        this.props.callbackOnChange(e.target.value);
    },
    render: function() {
        return (
            <TextField floatingLabelText="Search" fullWidth={true} onChange={this.handleOnChange} />
        );
    }
});


var Contact = React.createClass({
    handleOnClick: function(e) {
        this.props.callbackOnDelete(this.props.contactId);
    },
    render: function() {
        var contactDetailKey = function(id, service, label, handle) {
            return [id, service, label, handle].join(':');
        }

        var detailsDefined = !(this.props.details == null);

        var contactDetailNodes = (this.props.details || []).map(function (detail) {
            return (
                <TableRow key={contactDetailKey(this.props.contactId, detail.service, detail.label, detail.handle)}>
                    <TableRowColumn>{detail.service}</TableRowColumn>
                    <TableRowColumn>{detail.label}</TableRowColumn>
                    <TableRowColumn>{detail.handle}</TableRowColumn>
                </TableRow>
            );
        }.bind(this));

        var renderTable = function () {
            if (!detailsDefined) {
                return null;
            } else {
                return (
                    <Table>
                        <TableHeader>
                            <TableRow>
                                <TableHeaderColumn>Service</TableHeaderColumn>
                                <TableHeaderColumn>Label</TableHeaderColumn>
                                <TableHeaderColumn>Handle</TableHeaderColumn>
                            </TableRow>
                        </TableHeader>
                        <TableBody>
                            {contactDetailNodes}
                        </TableBody>
                    </Table>);
            }
        }.bind(this)();

        return (
            <Card>
                <CardHeader
                    title={this.props.name}
                    actAsExpander={detailsDefined}
                    showExpandableButton={detailsDefined}
                />
                <CardText expandable={detailsDefined}>
                    {renderTable}
                    <RaisedButton onMouseUp={this.handleOnClick} primary={true} label="DELETE" />
                </CardText>
            </Card>
        );
    }
});

var ContactForm = React.createClass({
    getInitialState: function() {
        return {
            name: ''
        };
    },
    handleOnChange: function(e) {
        this.setState({name: e.target.value});
    },
    handleOnSubmit: function(e) {
        var name = this.state.name;

        if (name == '') {
            return
        }

        this.props.callbackOnSubmit({
            name: name
        });
        this.setState({name: ''});
    },
    render: function(){
        var style={marginTop: '50px'}
        return (
            <div style={style}>
                <h2>Create Contact</h2>
                <TextField hintText="Name" onChange={this.handleOnChange} value={this.state.name} fullWidth={true}/>
                <FloatingActionButton onMouseUp={this.handleOnSubmit} >
                    <ContentAdd />
                </FloatingActionButton>
            </div>
        );
    }
});

module.exports = {
    ContactList: ContactList,
};