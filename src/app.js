var React = require('react');
var contacts = require('./contacts');

var ContactList = contacts.ContactList;

var App = React.createClass({
    render: function() {
        return (
            <ContactList url="http://localhost:3001/contacts" />
        );
    }
});

module.exports = App;